# Tutorial-Mediator

This is a yeoman generated mediator to get you started on mediator development.  To better understand the types of mediators [click here](http://openhim.readthedocs.io/en/latest/user-guide/mediators.html#mediator-types).

Send a message via openhim-api-curl

./openhim-api-curl.sh root@openhim.org password https://il-tutorial.cs300ohie.net:5000/encounters2/1 -u tutorial:pass