'use strict';

const request = require('request');
const utils = require('./auth');

exports.installMediatorChannels = (options, mediatorConfig, callback) => {
  // define login credentails for authorization
  const username = options.username;
  const password = options.password;

  const apiURL = options.apiURL;
  const rejectUnauthorized = !options.trustSelfSigned;

  // authenticate the username
  utils.authenticate({username, apiURL, rejectUnauthorized}, (err) => {
    if (err) {
      return callback(err);
    }
    const headers = utils.genAuthHeaders({username, password});

    console.log("register.js::headers: " + JSON.stringify(headers));
    console.log(`${apiURL}/mediators`);

    const reqOptions = {
      uri: `${apiURL}/mediators/${mediatorConfig.urn}/channels`,
      json: true,
      headers: headers,
      body: ['Tutorial Mediator'],
      rejectUnauthorized: rejectUnauthorized
    };

    request.post(reqOptions, (err,resp) => {
      if (err) {
        return callback(err);
      }

      if (resp.statusCode === 201) {
        console.log("Mediator: " + mediatorConfig.urn);
        console.log("Channel successfully installed.");
        callback();
      } else {
        callback(new Error(`Recieved a non-201 response code, the response body was: ${resp.body}`));
      }

    });
  });
};

