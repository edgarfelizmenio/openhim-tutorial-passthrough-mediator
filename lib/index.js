#!/usr/bin/env node
'use strict'

const express = require('express')
// const medUtils = require('openhim-mediator-utils')
const medUtils = require('./medUtils/medutils')
const winston = require('winston')

const utils = require('./utils')

// Logging setup
winston.remove(winston.transports.Console)
winston.add(winston.transports.Console, {level: 'info', timestamp: true, colorize: true})

// Config
var config = {} // this will vary depending on whats set in openhim-core
const apiConf = process.env.NODE_ENV === 'test' ? require('../config/test') : require('../config/config')
const mediatorConfig = require('../config/mediator')

var port = process.env.NODE_ENV === 'test' ? 7001 : mediatorConfig.endpoints[0].port

/**
 * setupApp - configures the http server for this mediator
 *
 * @return {express.App}  the configured http server
 */
function setupApp () {
  const app = express()
  var needle = require('needle');
  var async = require('async');


  // listen for requests coming through on /encounters/:id
  app.get('/encounters/:id', function (req, res) {
    needle.get('http://shr-tutorial.cs300ohie.net/encounters/' + req.params.id, function (err, resp) {
      if (err) {
        console.log(err);
        return;
      }
  
      /* ######################################### */
      /* ##### Create Initial Orchestration  ##### */
      /* ######################################### */
  
      // context object to store json objects
      var ctxObject = {};
      ctxObject['encounter'] = resp.body;
  
      //Capture 'encounter' orchestration data 
      var orchestrationsResults = [];
      orchestrationsResults.push({
        name: 'Get Encounter',
        request: {
          path : req.path,
          headers: req.headers,
          querystring: req.originalUrl.replace( req.path, "" ),
          body: req.body,
          method: req.method,
          timestamp: new Date().getTime()
        },
        response: {
          status: resp.statusCode,
          body: JSON.stringify(resp.body, null, 4),
          timestamp: new Date().getTime()
        }
      });
  
      /* ######################################### */
      /* ##### setup Orchestration Requests  ##### */
      /* ######################################### */

      // setup more orchestrations
      var orchestrations = [{ 
        ctxObjectRef: "client",
        name: "Get Client", 
        domain: "http://cr-tutorial.cs300ohie.net",
        path: "/patient/"+resp.body.patientId,
        params: "",
        body: "",
        method: "GET",
        headers: ""
      }, { 
        ctxObjectRef: "provider",
        name: "Get Provider", 
        domain: "http://hwr-tutorial.cs300ohie.net",
        path: "/providers/"+resp.body.providerId,
        params: "",
        body: "",
        method: "GET",
        headers: ""
      }];

      /* ###################################### */
      /* ##### setup Async Orch Requests  ##### */
      /* ###################################### */

      // start the async process to send requests
      async.each(orchestrations, function(orchestration, callback) {
          // construct the URL to request
          var orchUrl = orchestration.domain + orchestration.path + orchestration.params;

          // send the request to the orchestration
          needle.get(orchUrl, function(err, resp) {

            // if error occured
            if ( err ){
              callback(err);
            }

            // add new orchestration to object
            orchestrationsResults.push({
              name: orchestration.name,
              request: {
                path : orchestration.path,
                headers: orchestration.headers,
                querystring: orchestration.params,
                body: orchestration.body,
                method: orchestration.method,
                timestamp: new Date().getTime()
              },
              response: {
                status: resp.statusCode,
                body: JSON.stringify(resp.body, null, 4),
                timestamp: new Date().getTime()
              }
            });

            // add orchestration response to context object and return callback
            ctxObject[orchestration.ctxObjectRef] = resp.body;
            callback();
          });
        
        }, function(err){
        
          // This section will execute once all requests have been completed
          // if any errors occurred during a request the print out the error and stop processing
          if (err){
            console.log(err)
            return;
          }

          // Initial response object. We can actually work with this
          // var response = {
          //   status: resp.statusCode,
          //   headers: {
          //     'content-type': 'application/json'
          //   },
          //   body: JSON.stringify(resp.body, null, 4),
          //   timestamp: new Date().getTime()
          // };

          /* ############################ */
          /* ##### HTML conversion  ##### */
          /* ############################ */
          
          /* ##### Construct Encounter HTML  ##### */
          // first loop through all observations and build HTML rows
          var observationsHtml = '';
          for (var i = 0; i < ctxObject.encounter.observations.length; i++) { 
            observationsHtml += '    <tr>' + "\n" +
            '      <td>'+ctxObject.encounter.observations[i].obsType+'</td>' + "\n" +
            '      <td>'+ctxObject.encounter.observations[i].obsValue+'</td>' + "\n" +
            '      <td>'+ctxObject.encounter.observations[i].obsUnit+'</td>' + "\n" +
            '    </tr>' + "\n";
          }
      
          // setup the encounter HTML
          var healthRecordHtml = '  <h3>Patient ID: #'+ctxObject.encounter.patientId+'</h3>' + "\n" +
          '  <h3>Provider ID: #'+ctxObject.encounter.providerId+'</h3>' + "\n" +
          '  <h3>Encounter Type: '+ctxObject.encounter.encounterType+'</h3>' + "\n" +
          '  <h3>Encounter Date: '+ctxObject.encounter.encounterDate+'</h3>' + "\n" +
          '  <table cellpadding="10" border="1" style="border: 1px solid #000; border-collapse: collapse">' + "\n" +
          '    <tr>' + "\n" +
          '      <td>Type:</td>' + "\n" +
          '      <td>Value:</td>' + "\n" +
          '      <td>Unit:</td>' + "\n" +
          '    </tr>' + "\n" +
          observationsHtml + 
          '  </table>' + "\n";

          /* ##### Construct patient HTML  ##### */
          var patientRecordHtml = '  <h2>Patient Record: #'+ctxObject.client.patientId+'</h2>' + "\n" +
          '  <table cellpadding="10" border="1" style="border: 1px solid #000; border-collapse: collapse">' + "\n" +
          '    <tr>' + "\n" +
          '      <td>Given Name:</td>' + "\n" +
          '      <td>'+ctxObject.client.givenName+'</td>' + "\n" +
          '    </tr>' + "\n" +
          '    <tr>' + "\n" +
          '      <td>Family Name:</td>' + "\n" +
          '      <td>'+ctxObject.client.familyName+'</td>' + "\n" +
          '    </tr>' + "\n" +
          '    <tr>' + "\n" +
          '      <td>Gender:</td>' + "\n" +
          '      <td>'+ctxObject.client.gender+'</td>' + "\n" +
          '    </tr>' + "\n" +
          '    <tr>' + "\n" +
          '      <td>Phone Number:</td>' + "\n" +
          '      <td>'+ctxObject.client.phoneNumber+'</td>' + "\n" +
          '    </tr>' + "\n" +
          '  </table>' + "\n";
          
          
          /* ##### Construct provider HTML  ##### */
          var providerRecordHtml = '  <h2>Provider Record: #'+ctxObject.provider.providerId+'</h2>' + "\n" +
          '  <table cellpadding="10" border="1" style="border: 1px solid #000; border-collapse: collapse">' + "\n" +
          '    <tr>' + "\n" +
          '      <td>Title:</td>' + "\n" +
          '      <td>'+ctxObject.provider.title+'</td>' + "\n" +
          '    </tr>' + "\n" +
          '    <tr>' + "\n" +
          '      <td>Given Name:</td>' + "\n" +
          '      <td>'+ctxObject.provider.givenName+'</td>' + "\n" +
          '    </tr>' + "\n" +
          '    <tr>' + "\n" +
          '      <td>Family Name:</td>' + "\n" +
          '      <td>'+ctxObject.provider.familyName+'</td>' + "\n" +
          '    </tr>' + "\n" +
          '  </table>' + "\n";      

          // setup the main response body
          var responseBodyHtml = '<html>' + "\n" +
          '<body>' + "\n" +
          '  <h1>Health Record</h1>' + "\n" +
          healthRecordHtml +
          patientRecordHtml +
          providerRecordHtml + 
          '</body>' + "\n" +
          '</html>';

          /* ###################################### */
          /* ##### Construct Response Object  ##### */
          /* ###################################### */
      
          var urn = mediatorConfig.urn;
          var status = 'Successful';      
          var response = {
            status: resp.statusCode,
            headers: {
              'content-type': 'text/html'
            },
            body: responseBodyHtml,
            timestamp: new Date().getTime()
          }

          // construct property data to be returned - this can be anything interesting that you want to make available in core, or nothing at all
          var properties = {};
          properties[ctxObject.encounter.observations[0].obsType] = ctxObject.encounter.observations[0].obsValue + ctxObject.encounter.observations[0].obsUnit;
          properties[ctxObject.encounter.observations[1].obsType] = ctxObject.encounter.observations[1].obsValue + ctxObject.encounter.observations[1].obsUnit;
          properties[ctxObject.encounter.observations[2].obsType] = ctxObject.encounter.observations[2].obsValue + ctxObject.encounter.observations[2].obsUnit;
          properties[ctxObject.encounter.observations[3].obsType] = ctxObject.encounter.observations[3].obsValue + ctxObject.encounter.observations[3].obsUnit;
          properties[ctxObject.encounter.observations[4].obsType] = ctxObject.encounter.observations[4].obsValue + ctxObject.encounter.observations[4].obsUnit;
          properties[ctxObject.encounter.observations[5].obsType] = ctxObject.encounter.observations[5].obsValue + ctxObject.encounter.observations[5].obsUnit;
      
          properties[ctxObject.client.givenName + ' ' + ctxObject.client.familyName + '(' + ctxObject.client.gender + ')'] = ctxObject.client.phoneNumber;
          properties[ctxObject.provider.title] = ctxObject.provider.givenName + ' ' + ctxObject.provider.familyName;

          // construct returnObject to be returned
          var returnObject = {
            "x-mediator-urn": urn,
            "status": status,
            "response": response,
            "orchestrations": orchestrationsResults,
            "properties": properties
          }
      
          // set content type header so that OpenHIM knows how to handle the response
          res.set('Content-Type', 'application/json+openhim');
          res.send(returnObject);
      });
    });
  })




  // app.get('/encounters/:id', function (req, res) {
  //   var responseBody = req.params.id
  //   var headers = { 'content-type': 'application/json' }
  //   let orchestrations = [
  //     utils.buildOrchestration('Primary Route', new Date().getTime(), req.method, req.url, req.headers, req.body,
  //     {statusCode: 200, headers: headers},
  //     responseBody)
  //   ]
  //   res.set('Content-Type', 'application/json+openhim')
  //   var properties = {property: 'Primary Route'}
  //   console.log("lodi lodi")
  //   res.send(utils.buildReturnObject(mediatorConfig.urn, 'Successful', 200, headers, responseBody, orchestrations, properties))
  // })

  /*
  app.all('*', (req, res) => {
    winston.info(`Processing ${req.method} request on ${req.url}`)
    var responseBody = 'Primary Route Reached'
    var headers = { 'content-type': 'application/json' }

    // add logic to alter the request here

    // capture orchestration data
    var orchestrationResponse = { statusCode: 200, headers: headers }
    let orchestrations = []
    orchestrations.push(utils.buildOrchestration('Primary Route', new Date().getTime(), req.method, req.url, req.headers, req.body, orchestrationResponse, responseBody))

    // set content type header so that OpenHIM knows how to handle the response
    res.set('Content-Type', 'application/json+openhim')

    // construct return object
    var properties = { property: 'Primary Route' }
    res.send(utils.buildReturnObject(mediatorConfig.urn, 'Successful', 200, headers, responseBody, orchestrations, properties))
  })*/
  return app
}

/**
 * start - starts the mediator
 *
 * @param  {Function} callback a node style callback that is called once the
 * server is started
 */
function start (callback) {
  if (apiConf.api.trustSelfSigned) { process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0' }

  if (apiConf.register) {
    medUtils.registerMediator(apiConf.api, mediatorConfig, (res,err) => {
      winston.info(res)
      if (err) {
        winston.error('Failed to register this mediator, check your config')
        winston.error(err.stack)
        process.exit(1)
      }
      apiConf.api.urn = mediatorConfig.urn
      medUtils.fetchConfig(apiConf.api, (err, newConfig) => {
        winston.info('Received initial config:')
        winston.info(JSON.stringify(newConfig))
        config = newConfig
        if (err) {
          winston.error('Failed to fetch initial config')
          winston.error(err.stack)
          process.exit(1)
        } else {
          winston.info('Successfully registered mediator!')
          
          // activate heartbeat
          let app = setupApp()
          const server = app.listen(port, () => {
            if (apiConf.heartbeat) {
              let configEmitter = medUtils.activateHeartbeat(apiConf.api)
              configEmitter.on('config', (newConfig) => {
                winston.info('Received updated config:')
                winston.info(JSON.stringify(newConfig))
                // set new config for mediator
                config = newConfig

                // we can act on the new config received from the OpenHIM here
                winston.info(config)
              })
            }
            callback(server)
          })
          // install channel
          medUtils.installMediatorChannels(apiConf.api, mediatorConfig, (res,err) => {
            winston.info(res)
            if (err) {
              winston.error('Failed to register mediator channels, check your config')
              winston.error(err.stack)
              process.exit(1)
            }
          });
        }
      })
    })
  } else {
    // default to config from mediator registration
    config = mediatorConfig.config
    let app = setupApp()
    const server = app.listen(port, () => callback(server))
  }
}
exports.start = start

if (!module.parent) {
  // if this script is run directly, start the server
  start(() => winston.info(`Listening on ${port}...`))
}
