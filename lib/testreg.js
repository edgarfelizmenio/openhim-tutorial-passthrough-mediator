const utils = require('openhim-mediator-utils');


options = {
	"apiURL": "https://il-tutorial.cs300ohie.net:8080",
	"username": "root@openhim.org",
	"password": "password"
}

mediator = {
	"urn": "urn:cs300openhie:test-registration",
	"version": "0.0.1",
	"name": "reg tester",
	"defaultChannelConfig": [
		{
			"name": "reg-tester",
			"urlPattern": "/reg/.*",
			"alerts": [],
			"txRerunAcl" : [],
			"txViewFullAcl": [],
			"txViewAcl": [],
			"properties": [],
			"matchContentTypes": [],
			"routes": [
				{
					"name": "Reg Tester",
					"host": "med-tutorial.cs300ohie.net",
					"port": "4000",
					"primary": true,
					"type":"http"
					
				}
			],
			"allow": ["tut"],
			"type": "http"
		}
	],
	"endpoints": [
		{"name": "reg tester route",
		"hosts": "med-tutorial.cs300ohie.net",
		"path": "/",
		"port": "4000",
		"primary": true,
		"type":  "http"}
	]
}



utils.registerMediator(options, mediator, function(err) {
	if (err) {
		console.log(err);	  
	} else {
		console.log("Successful Registration");
	}
});
